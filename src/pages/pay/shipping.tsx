import { menuController } from '@ionic/core';
import { IonBackButton, IonBadge, IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCheckbox, IonCol, IonContent, IonFab, IonFabButton, IonGrid, IonHeader, IonIcon, IonImg, IonInput, IonItem, IonItemDivider, IonItemOption, IonItemOptions, IonItemSliding, IonLabel, IonList, IonPage, IonRadio, IonRow, IonSegment, IonSegmentButton, IonSlide, IonSlides, IonTabBar, IonTabButton, IonTabs, IonText, IonTitle, IonToggle, IonToolbar } from '@ionic/react';
import { add, arrowForward, bagHandle, bagHandleOutline, basket, calendar, call, card, cart, checkmark, checkmarkCircleOutline, chevronBack, chevronForward, colorFill, ellipse, globe, heart, heartCircle, heartDislike, home, informationCircle, key, lockClosed, logoFacebook, mail, mailOpen, map, menuSharp, people, peopleCircle, person, personCircle, pin, pricetag, remove, search, star } from 'ionicons/icons';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import './shipping.css';

const Shipping: React.FC = () => {
  const history = useHistory();
  const [content, setContent] = useState('shipping');
  const [e, setE] = useState('shipping');


  const slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  return (
    <IonPage >
      <IonHeader id=''>
        <IonToolbar id=''>
          <IonButton slot='start' id='buttonHeaderScreen6'>
            <IonIcon id='iconHeader' icon={chevronBack}></IonIcon>
          </IonButton>
          <IonTitle>
            <div className='ion-text-left'><IonText><b>Shipping address</b></IonText></div>
          </IonTitle>
          <IonButton slot='end' id='buttonHeaderScreen6' >
            <IonIcon icon={search}></IonIcon>
          </IonButton>
          <IonButton slot='end' id='buttonHeaderScreen6'>
            <IonIcon icon={bagHandle}></IonIcon>
          </IonButton>
          <IonButton slot='end' id='buttonHeaderScreen6' >
            <IonIcon icon={personCircle}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
          {/* Segment */}
          <div id='segment'>
            <IonSegment value={e} color='dark' onIonChange={e => setE(e.detail.value!)}>
              <IonSegmentButton value="shipping" >
                <IonLabel>Shipping</IonLabel>
              </IonSegmentButton>
              <IonSegmentButton value="payment">
                <IonLabel>Payment</IonLabel>
              </IonSegmentButton>
              <IonSegmentButton value="OrderPlaced">
                <IonLabel>Order placed</IonLabel>
              </IonSegmentButton>
            </IonSegment>
          </div>

          {e === 'shipping' ?
            <div id='divtextShipping'>
              <IonRow>
                <IonCol size='0.5'></IonCol>
                <IonCol size='10.5'> <IonText id='textShipping'><b>Shipping</b></IonText></IonCol>
                <IonCol size='1'></IonCol>
              </IonRow>
              <IonRow>
                <IonCol size='1'></IonCol>
                <IonCol size='10'>
                  <IonItem id='item2Shipping'>
                    <IonLabel position="floating" color='dark'>Full name</IonLabel>
                    <IonInput id='inputTextShipping'></IonInput>
                  </IonItem>
                  <IonItem id='item2Shipping'>
                    <IonLabel position="floating" color='dark'>Address</IonLabel>
                    <IonInput id='inputTextShipping'></IonInput>
                  </IonItem>
                  <IonItem id='item2Shipping'>
                    <IonLabel position="floating" color='dark'>Pin Code</IonLabel>
                    <IonInput id='inputTextShipping'></IonInput>
                  </IonItem>
                  <IonItem id='item2Shipping'>
                    <IonLabel position="floating" color='dark'>Mobile Number</IonLabel>
                    <IonInput id='inputTextShipping'></IonInput>
                  </IonItem>
                  <IonItem id='item2Shipping'>
                    <IonLabel position="floating" color='dark'>City</IonLabel>
                    <IonInput id='inputTextShipping'></IonInput>
                  </IonItem>
                  <IonItem id='item2Shipping'>
                    <IonLabel position="floating" color='dark'>Country</IonLabel>
                    <IonInput id='inputTextShipping'></IonInput>
                  </IonItem>
                </IonCol>
                <IonCol size='1'></IonCol>
              </IonRow>
              <IonRow>
                <IonCol size='1'></IonCol>
                <IonCol size='10'>
                  <IonButton color='dark' expand="block" fill="solid" shape="round" id='butonAddCart' size='large' onClick={() => setE('payment')}>
                    <div className="textButtonProductPreview">
                      <b>Continue to pay</b>
                    </div>
                  </IonButton>
                </IonCol>
                <IonCol size='1'></IonCol>
              </IonRow>
            </div>

            : e == 'payment' ?
              <div id='divtextShipping'>
                <IonRow>
                  <IonCol size='0.5'></IonCol>
                  <IonCol size='10.5'> <IonText id='textShipping'><b>Payment</b></IonText></IonCol>
                  <IonCol size='1'></IonCol>
                </IonRow>
                <IonRow>
                  <IonCol size='1'></IonCol>
                  <IonCol size='10'>
                    <div id='target'>
                      <IonRow>
                        <IonCol id='textTarget2' size='1'></IonCol>
                        <IonCol id='textTarget' size='5'><IonText>Credit Card</IonText></IonCol>
                        <IonCol id='textTarget' size='5'><IonText>Back Name</IonText></IonCol>
                        <IonCol id='textTarget2' size='1'></IonCol>
                      </IonRow>
                      <IonRow id='itemImg'>
                        <IonItem id='itemImg' color='primary-contrast'>
                          <IonImg id='imgChip' src='assets/chip.png'></IonImg>
                        </IonItem>
                      </IonRow>
                      <IonRow>
                        <IonCol size='1'></IonCol>
                        <IonCol size='10'>
                          <div className='ion-text-center' id='numbertarjet'>
                            <IonInput type='number' placeholder='1234 5678 9012 3456' value='1234 5678 9012 3456' id='inputTargetShipping'></IonInput>
                          </div>
                        </IonCol>
                        <IonCol size='1'></IonCol>
                      </IonRow>
                      <IonRow>
                        <IonCol size='2'></IonCol>
                        <IonCol size='5'><IonText id='codeTarget'>0123</IonText></IonCol>
                        <IonCol size='3'><div className='ion-text-right' ><IonText id='fechaTarget' color='light'>01/08</IonText></div></IonCol>
                      </IonRow>
                      <IonRow>
                        <IonCol id='' size='1'></IonCol>
                        <IonCol id='textTarget2' size='10'>
                          <div className='ion-text-left' id='numbertarjet'>
                            <IonInput type='text' placeholder='Name Surname'>
                            </IonInput>
                          </div>
                        </IonCol>
                        <IonCol id='' size=''></IonCol>
                        <IonCol id='' size='1'></IonCol>
                      </IonRow>
                    </div>
                  </IonCol>
                  <IonCol size='1'></IonCol>
                </IonRow>

                {/* Button */}
                <IonRow>
                  <IonCol size='1'></IonCol>
                  <IonCol size='10'>
                    <IonButton color='dark' expand="block" fill="solid" shape="round" id='butonAddCart' size='large' onClick={() => history.push('')}>
                      <div className="textButtonProductPreview">
                        <b>Cash On Delibery</b>
                      </div>
                    </IonButton>
                  </IonCol>
                  <IonCol size='1'></IonCol>
                </IonRow>
                {/* Button */}
                <IonRow>
                  <IonCol size='1'></IonCol>
                  <IonCol size='10'>
                    <IonButton color='dark' expand="block" fill="solid" shape="round" id='btnUPIShippingAddress' size='large' onClick={() => history.push('')}>
                      <div className="textButtonProductPreview">
                        <b>Pay through UPI</b>
                      </div>
                    </IonButton>
                  </IonCol>
                  <IonCol size='1'></IonCol>
                </IonRow>
                {/* Button */}
                <IonRow>
                  <IonCol size='1'></IonCol>
                  <IonCol size='10'>
                    <IonButton color='dark' expand="block" fill="solid" shape="round" id='btnPayNowShippingAddress' size='large' onClick={() => history.push('')}>
                      <div className="textButtonProductPreview">
                        <b>Pay Now $ {75}</b>
                      </div>
                    </IonButton>
                  </IonCol>
                  <IonCol size='1'></IonCol>
                </IonRow>
              </div> :
              <div className='ion-text-center'>
                <IonRow>
                  <IonCol size='2' ></IonCol>
                  <IonCol size='8' >
                    <IonIcon id='iconMark' icon={checkmarkCircleOutline} color='tertiary'></IonIcon>
                  </IonCol>
                  <IonCol size='2' ></IonCol>
                </IonRow>
                <IonRow>
                  <IonCol size='1'></IonCol>
                  <IonCol size='10'>
                  <IonText id='IonText' ><b>Your Order Is Confirmed</b></IonText>
                  </IonCol>
                  <IonCol size='1'></IonCol>
                </IonRow>
                <IonRow>
                <IonCol size='1'></IonCol>
                  <IonCol size='10'>
                  <IonText id='IonText'>Thank For Your Order</IonText>
                  </IonCol>
                  <IonCol size='1'></IonCol>
                </IonRow>
                <IonRow>
                  <IonCol size='1'></IonCol>
                  <IonCol size='10'>
                    <IonButton color='dark' expand="block" fill="solid" shape="round" id='btnPayNowShippingAddress' size='large'>
                      <div className="textButtonProductPreview">
                        <b>Done</b>
                      </div>
                    </IonButton>
                  </IonCol>
                  <IonCol size='1'></IonCol>
                </IonRow>
                <IonText color='tertiary'><b>Back to Home</b></IonText>
              </div>
          }

        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Shipping;

function toggleFavorite(): void {
  throw new Error('Function not implemented.');
}
