import { menuController } from '@ionic/core';
import { IonBackButton, IonBadge, IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCheckbox, IonCol, IonContent, IonFab, IonFabButton, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonItemDivider, IonItemOption, IonItemOptions, IonItemSliding, IonLabel, IonList, IonPage, IonRadio, IonRow, IonSegment, IonSegmentButton, IonSlide, IonSlides, IonTabBar, IonTabButton, IonTabs, IonText, IonTitle, IonToggle, IonToolbar } from '@ionic/react';
import { add, arrowForward, bagHandle, bagHandleOutline, basket, calendar, call, card, cart, chevronBack, chevronForward, colorFill, ellipse, globe, heart, heartCircle, heartDislike, home, informationCircle, key, lockClosed, logoFacebook, mail, mailOpen, map, menuSharp, people, peopleCircle, person, personCircle, pin, pricetag, remove, search, star } from 'ionicons/icons';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import './cardList.css';

const CartList: React.FC = () => {
  const history = useHistory();

  const [count, setCount] = useState(0);
  const [quantity2, setQuantity2] = useState(0);
  const [quantity3, setQuantity3] = useState(0);

  let precio1 = 25;
  let precio2 = 45;
  let precio3 = 20;

  let total = (precio1 * count) + (precio2 * quantity2) + (precio3 * quantity3);
  let discount = ((16.66 * total)/100);
  let pay = total - discount;

  const slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  return (
    <IonPage >
      <IonHeader id=''>
        <IonToolbar id=''>
          <IonButton slot='start' id='buttonHeaderScreen6'>
            <IonIcon id='iconHeader' icon={chevronBack}></IonIcon>
          </IonButton>
          <IonTitle>
            <div className='ion-text-center'><IonText><b>Cart</b></IonText></div>
          </IonTitle>
          <IonButton slot='end' id='buttonHeaderScreen6' >
            <IonIcon icon={search}></IonIcon>
          </IonButton>
          <IonButton slot='end' id='buttonHeaderScreen6'>
            <IonIcon icon={bagHandle}></IonIcon>
          </IonButton>
          <IonButton slot='end' id='buttonHeaderScreen6' >
            <IonIcon icon={personCircle}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
         {/* Card */}
          <IonRow>
            <IonCard id='card'>
              <IonRow>
                <IonCol size='2'>
                  <img src='assets/img2.jpg' />
                </IonCol>
                <IonCol size='7'>
                  <IonCardHeader id='cardHeader'>
                    <IonCardTitle id='cardTitle'> Women Dark Blue Dungaare </IonCardTitle>
                    <IonCardSubtitle> Size: M </IonCardSubtitle>
                    <IonCardSubtitle>$ {precio1 * count} </IonCardSubtitle>
                  </IonCardHeader>
                </IonCol>
                <IonCol size='3'>
                  <div className='divButtons'>

                  <IonButton id='btnAddRemove' fill='outline' color='dark' shape='round' size='small' 
                  onClick={() => setCount(count - 1)}>
                    <IonIcon icon={remove}></IonIcon>
                  </IonButton>
                    <IonText id='numberProducts'>{count}</IonText>
                  <IonButton id='btnAddRemove' fill='outline' color='dark' shape='round' size='small'
                  onClick={() => setCount(count + 1)}>
                    <IonIcon icon={add}></IonIcon>
                  </IonButton>
                  </div>
                </IonCol>
              </IonRow>
            </IonCard>
          </IonRow>
         {/* Card */}
          <IonRow>
            <IonCard id='card'>
              <IonRow>
                <IonCol size='2'>
                  <img src='assets/img1.jpg' />
                </IonCol>
                <IonCol size='7'>
                  <IonCardHeader id='cardHeader'>
                    <IonCardTitle id='cardTitle'> Women Dark Blue Dungaare </IonCardTitle>
                    <IonCardSubtitle> Size: M </IonCardSubtitle>
                    <IonCardSubtitle> $ {precio2 * quantity2} </IonCardSubtitle>
                  </IonCardHeader>
                </IonCol>
                <IonCol size='3'>
                  <div className='divButtons'>

                  <IonButton id='btnAddRemove' fill='outline' color='dark' shape='round' size='small'
                  onClick={() => setQuantity2(quantity2 - 1)}>
                    <IonIcon icon={remove}></IonIcon>
                  </IonButton>
                    <IonText id='numberProducts'>{quantity2}</IonText>
                  <IonButton id='btnAddRemove' fill='outline' color='dark' shape='round' size='small'
                  onClick={() => setQuantity2(quantity2 + 1)}>
                    <IonIcon icon={add}></IonIcon>
                  </IonButton>
                  </div>
                </IonCol>
              </IonRow>
            </IonCard>
          </IonRow>
         {/* Card */}
          <IonRow>
            <IonCard id='card'>
              <IonRow>
                <IonCol size='2'>
                  <img src='assets/img7.jpg' />
                </IonCol>
                <IonCol size='7'>
                  <IonCardHeader id='cardHeader'>
                    <IonCardTitle id='cardTitle'> Women Dark Blue Dungaare </IonCardTitle>
                    <IonCardSubtitle> Size: M </IonCardSubtitle>
                    <IonCardSubtitle> $ {precio3 * quantity3} </IonCardSubtitle>
                  </IonCardHeader>
                </IonCol>
                <IonCol size='3'>
                  <div className='divButtons'>

                  <IonButton id='btnAddRemove' fill='outline' color='dark' shape='round' size='small'
                  onClick={() => setQuantity3(quantity3 - 1)}>
                    <IonIcon icon={remove}></IonIcon>
                  </IonButton>
                    <IonText id='numberProducts'>{ quantity3 }</IonText>
                  <IonButton id='btnAddRemove' fill='outline' color='dark' shape='round' size='small'
                  onClick={() => setQuantity3(quantity3 + 1)}>
                    <IonIcon icon={add}></IonIcon>
                  </IonButton>
                  </div>
                </IonCol>
              </IonRow>
            </IonCard>
          </IonRow>

          {/* Card Total */}
          <IonRow>
            <IonCol size='1'></IonCol>
            <IonCol size='10'>
              <IonCard>
                <IonCardHeader>
                  <IonRow>
                    <IonCol size='9'>
                    <IonText>Total Ammount</IonText>
                    </IonCol>
                    <IonCol size='3'>
                    <IonText>$ { total }</IonText>
                    </IonCol>
                  </IonRow>
                  <IonRow>
                    <IonCol size='9'>
                    <IonText>Discount</IonText> <IonIcon icon={pricetag}></IonIcon>
                    </IonCol>
                    <IonCol size='3'>
                    <IonText>$ { discount.toFixed(2) }</IonText>
                    </IonCol>
                  </IonRow>
                </IonCardHeader>
                <IonCardContent>
                  <IonRow>
                    <IonCol size='9'><IonText>Ammount payable</IonText></IonCol>
                    <IonCol size='3'> <b>$ { pay.toFixed(2) }</b> </IonCol>
                  </IonRow>
                </IonCardContent>
              </IonCard>
            </IonCol>
            <IonCol size='1'></IonCol>
          </IonRow>

          {/* button */}
          <IonRow>
            <IonCol size='1'></IonCol>
            <IonCol size='10'>
              <IonButton color='dark' expand="block" fill="solid" shape="round" id='butonAddCart' size='large' onClick={() => history.push('/shipping')}>
                <IonIcon icon={cart} size='default'></IonIcon>
                <div className="textButtonProductPreview">
                  <b>Checkout Security</b>
                </div>
              </IonButton>
            </IonCol>
            <IonCol size='1'></IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default CartList;

function toggleFavorite(): void {
  throw new Error('Function not implemented.');
}
