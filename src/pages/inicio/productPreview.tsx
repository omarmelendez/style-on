import { menuController } from '@ionic/core';
import { IonBackButton, IonBadge, IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCheckbox, IonCol, IonContent, IonFab, IonFabButton, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonItemDivider, IonLabel, IonPage, IonRow, IonSegment, IonSegmentButton, IonSlide, IonSlides, IonTabBar, IonTabButton, IonTabs, IonText, IonTitle, IonToolbar } from '@ionic/react';
import { add, arrowForward, bagHandle, bagHandleOutline, basket, calendar, call, card, cart, chevronBack, chevronForward, colorFill, ellipse, globe, heart, heartCircle, heartDislike, home, informationCircle, key, lockClosed, logoFacebook, mail, mailOpen, map, menuSharp, people, peopleCircle, person, personCircle, pin, search, star } from 'ionicons/icons';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import './productPreview.css';

const ProductPreview: React.FC = () => {
  const history = useHistory();

  const slideOpts = {
    initialSlide: 1,
    speed: 400
  };

  return (
    <IonPage >
      <IonHeader id=''>
        <IonToolbar id=''>
          <IonButton slot='start' id='buttonHeaderScreen6'>
            <IonIcon id='iconHeader' icon={chevronBack}></IonIcon>
          </IonButton>
          <IonTitle>
            <div className='ion-text-center'><IonText><b>Preview</b></IonText></div>            
          </IonTitle>
          <IonButton slot='end' id='buttonHeaderScreen6' >
            <IonIcon icon={search}></IonIcon>
          </IonButton>
          <IonButton slot='end' id='buttonHeaderScreen6'>
            <IonIcon icon={bagHandle}></IonIcon>
          </IonButton>
          <IonButton slot='end' id='buttonHeaderScreen6' >
            <IonIcon icon={personCircle}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <div id='divSlideContent'>
          <IonFab id='buttonFav' slot='end' horizontal='end'>
            <IonFabButton size='small' color='light' translucent={false} show={false}>
              <IonIcon size='large' color='danger' icon={heart}>
              </IonIcon>
            </IonFabButton>
          </IonFab>
          <IonSlides id='slide' pager={true} options={slideOpts}>
            <IonSlide id='slideContent'>
              <img id='img' src='assets/img4.jpg' />
            </IonSlide>
            <IonSlide id='slideContent'>
              <img id='img' src='assets/img2.jpg' />
            </IonSlide>
            <IonSlide id='slideContent'>
              <img id='img' src='assets/img3.jpg' />
            </IonSlide>
          </IonSlides>
        </div>
        <IonGrid>
          <div className='ion-text-center'>
            <IonText><b>Casual Puff Sleeve solid Women Red Top</b></IonText>
          </div>
          <IonRow>
            <IonCol size='1'>

            </IonCol>
            <IonCol size='5'>
              <IonText id='textColorSize'>Color:</IonText>
              <IonItem id='op'>
                <div>
                  <IonIcon id='iconEllipse' icon={ellipse} size='large'></IonIcon>
                  <IonIcon id='iconEllipse' icon={ellipse} color='danger' size='large'></IonIcon>
                  <IonIcon id='iconEllipse' icon={ellipse} color='secondary' size='large'></IonIcon>
                </div>
              </IonItem>
            </IonCol>
            <IonCol size='6'>
            <IonText id='textColorSize'>Select size:</IonText>
              <IonItem id='op'>
                <div>
                  <IonText id='circleSizeS2'>S</IonText>
                  <IonText id='circleSize'>M</IonText>
                  <IonText id='circleSize'>L</IonText>
                </div>
              </IonItem>
            </IonCol>
            <IonCol size='0'>

            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol size='1'></IonCol>
            <IonCol size='10'>
              <IonButton color='dark' expand="block" fill="solid" shape="round" id='butonAddCart' size='large' onClick={() => history.push('/card-List')}>
                <IonIcon icon={cart} size='default'></IonIcon>
                <div className="textButtonProductPreview">
                  <b>Add Cart</b>
                </div>
              </IonButton>
            </IonCol>
            <IonCol size='1'></IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default ProductPreview;

function toggleFavorite(): void {
  throw new Error('Function not implemented.');
}
