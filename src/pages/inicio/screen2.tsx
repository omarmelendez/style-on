import { IonBackButton, IonButton, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonPage, IonRow, IonText, IonTitle, IonToolbar } from '@ionic/react';
import { arrowForward, chevronForward, logoFacebook, mail, mailOpen, star } from 'ionicons/icons';
import { useHistory } from 'react-router';
import './screen2.css';

const Screen2: React.FC = () => {
  const history = useHistory();
  return (
    <IonPage >
      <IonHeader id='headerScreen2'>
        <IonToolbar id='toolBarScreen2'>
        <IonButton slot='end' id='buttonHeaderScreen2' onClick={() => history.push('/Screen3')}>
          <IonText><b>Skip</b></IonText> <IonIcon icon={chevronForward}></IonIcon>
        </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <div id='backScreen2'>
        <IonGrid>

          <IonRow>
            <IonCol size="0"></IonCol>
              <IonCol size="6">
                <div className='textScreen2'>
                  Style<b>ON</b>
                </div>
              </IonCol>
            <IonCol size="6"></IonCol>
          </IonRow>

          <IonRow>
            <IonCol size="1"></IonCol>
              <IonCol size="8">
                <div className='text2Screen2'>
                  Sign up now <br /> get 30% cashback on first purchase  
                </div>
              </IonCol>
            <IonCol size="3"></IonCol>
          </IonRow>

          <IonRow slot=''>
            <IonCol size="1"></IonCol>
            <IonCol size="10">
              <IonButton className='IonButtonScreen2' expand="block" fill="solid" shape="round" id='buton' size='default'>
                <div className="textButtonScreen2">
                  <b>Sign In</b>
                </div>
              </IonButton>
              <IonButton className='IonButtonScreen2' expand="block" fill="solid" shape="round" id='buton' size='default'>
                <div className="textButtonScreen2">
                  <b>Sign Up With Email</b>
                </div>
                  <IonIcon slot="start" icon={mail} /> 
              </IonButton>
              <IonButton className='IonButtonScreen2' expand="block" fill="solid" shape="round" id='buton' size='default' color='tertiary'>
                <div className="textButtonScreen2">
                  <b>Log In With Facebook</b>
                </div>
                  <IonIcon slot="start" icon={logoFacebook} /> 
              </IonButton>
            </IonCol>
            <IonCol size="1"></IonCol>
          </IonRow>
        </IonGrid>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Screen2;
