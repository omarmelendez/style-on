import { IonBackButton, IonButton, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonItemDivider, IonLabel, IonPage, IonRow, IonText, IonTitle, IonToolbar } from '@ionic/react';
import { arrowForward, chevronBack, chevronForward, key, lockClosed, logoFacebook, mail, mailOpen, people, person, star } from 'ionicons/icons';
import { useState } from 'react';
import { useHistory } from 'react-router';
import './screen3.css';

const Screen3: React.FC = () => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [formSubmitted, setFormSubmitted] = useState(false);
  const [usernameError, setUsernameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const history = useHistory();

  const login = async (e: React.FormEvent) => {
    e.preventDefault();
    setFormSubmitted(true);
    if(!username) {
      setUsernameError(true);
    }
    if(!password) {
      setPasswordError(true);
    }

    if(username && password) {
      await setIsLoggedIn(true);
      await setUsernameAction(username);
      history.push('/Screen6', {direction: 'none'});
    }
  };

  return (
    <IonPage >
      <IonHeader id='headerScreen3'>
        <IonToolbar id='toolBarScreen3'>
          <IonButton slot='start' id='buttonHeaderScreen3' onClick={() => history.push('/Screen2')}>
            <IonIcon icon={chevronBack}></IonIcon>
          </IonButton>
          <IonButton slot='end' id='buttonHeaderScreen3' onClick={() => history.push('/Screen4')}>
            <IonText><b>Skip</b></IonText> <IonIcon icon={chevronForward}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <div id='backScreen3'>
          <IonGrid>

            <IonRow>
              <IonCol size="1"></IonCol>
              <IonCol size="10">
                <div className='textScreen3'>
                  Style<b>ON</b>
                </div>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>
            <div id='divtext'>
              <IonRow>
                <IonCol size="1"></IonCol>
                <IonCol size="10">
                  <div className='text2Screen3'>
                    Create Account
                  </div>
                </IonCol>
                <IonCol size="1"></IonCol>
              </IonRow>
            </div>

            <IonRow slot=''>
              <IonCol size="1"></IonCol>
              <IonCol size="10">
                {/* username */}
                <IonItem id='item' color='transparent'>
                  <IonLabel id='label' color='light'><IonIcon icon={person}></IonIcon></IonLabel>
                  <IonInput placeholder='Name' value={username} id='inputText' onIonChange={e => { setUsername(e.detail.value!); setUsernameError(false); }}></IonInput>
                </IonItem>

                {formSubmitted && usernameError && <IonText color="danger">
                  <p className="ion-padding-start">
                    Username is required
                  </p>
                </IonText>}

                {/* email */}
                <IonItem id='item' color='white'>
                  <IonLabel id='label' color='light'><IonIcon icon={mail}></IonIcon></IonLabel>
                  <IonInput placeholder='E-mail' id='inputText' value={email} onIonChange={e => { setEmail(e.detail.value!); setEmailError(false); }}></IonInput>
                </IonItem>

                {formSubmitted && emailError && <IonText color="danger">
                  <p className="ion-padding-start">
                    E-mail is required
                  </p>
                </IonText>}

                {/* Password */}
                <IonItem id='item' color='white'>
                  <IonLabel id='label' color='light'><IonIcon icon={lockClosed}></IonIcon></IonLabel>
                  <IonInput color='light' placeholder='Password' id='inputText' value={password} onIonChange={e => { setPassword(e.detail.value!); setPasswordError(false); }}></IonInput>
                </IonItem>

                {formSubmitted && passwordError && <IonText color="danger">
                  <p className="ion-padding-start">
                    Password is required
                  </p>
                </IonText>}

                <IonButton type='submit' className='IonButtonScreen3' expand="block" fill="solid" shape="round" id='buton' size='default'>
                  <div className="textButtonScreen3">
                    <b>Sign In</b>
                  </div>
                </IonButton>
                <IonButton className='IonButtonScreen3' expand="block" fill="solid" shape="round" id='buton' size='default' color='tertiary'>
                  <div className="textButtonScreen3">
                    <b>Log In With Facebook</b>
                  </div>
                  <IonIcon slot="start" icon={logoFacebook} />
                </IonButton>
                <div className='ion-text-center'>
                  <IonText id='textend'>Already a account? <b>Log in</b></IonText>
                </div>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>
          </IonGrid>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Screen3;
function setIsLoggedIn(arg0: boolean) {
  throw new Error('Function not implemented.');
}

function setUsernameAction(username: string) {
  throw new Error('Function not implemented.');
}

