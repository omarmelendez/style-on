import { IonBackButton, IonButton, IonCheckbox, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonItemDivider, IonLabel, IonPage, IonRow, IonText, IonTitle, IonToolbar } from '@ionic/react';
import { arrowForward, chevronBack, chevronForward, key, lockClosed, logoFacebook, mail, mailOpen, people, person, star } from 'ionicons/icons';
import { useState } from 'react';
import { useHistory } from 'react-router';
import './screen5.css';

const checkboxList = [
  { val: 'Pepperoni', isChecked: true },
  { val: 'Sausage', isChecked: false },
  { val: 'Mushroom', isChecked: false }
];

const Screen5: React.FC = () => {
  const [checked, setChecked] = useState(false);
  const history = useHistory();
  return (
    <IonPage >
      <IonHeader id='headerScreen5'>
        <IonToolbar id='toolBarScreen5'>
          <IonButton slot='start' id='buttonHeaderScreen5' onClick={() => history.push('/Screen4')}>
            <IonIcon icon={chevronBack}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <div id='backScreen5'>
          <IonGrid>

            <IonRow>
              <IonCol size="1"></IonCol>
              <IonCol size="10">
                <div className='textScreen5'>
                  Style<b>ON</b>
                </div>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>
            <div id='divtextScreen5'>
              <IonRow>
                <IonCol size="1"></IonCol>
                <IonCol size="10">
                  <div className='text2Screen5'>
                    <b>Sign In</b>
                  </div>
                </IonCol>
                <IonCol size="1"></IonCol>
              </IonRow>
            </div>

            <IonRow slot=''>
              <IonCol size="1"></IonCol>
              <IonCol size="10">
                <IonItem id='itemScreen5' color='white'>
                  <IonLabel id='labelScreen5' color='light'><IonIcon icon={mail}></IonIcon></IonLabel>
                  <IonInput placeholder='E-mail' id='inputText'></IonInput>
                </IonItem>
                <IonItem id='itemScreen5' color='white'>
                  <IonLabel id='labelScreen5' color='light'><IonIcon icon={lockClosed}></IonIcon></IonLabel>
                  <IonInput color='light' placeholder='Password' id='inputText'></IonInput>
                </IonItem>
                <IonRow>
                  <IonCol size='6'>
                    <IonItem id='item2Screen5'>
                      {/* <IonLabel id='labelScreen5'>Checked: {JSON.stringify(checked)}</IonLabel> */}
                      <IonLabel id='labelScreen5'>Remember me</IonLabel>
                      <IonCheckbox id='checkBox' slot='start' checked={checked} onIonChange={e => setChecked(e.detail.checked)} />
                    </IonItem>
                  </IonCol>
                  <IonCol size='6'>
                    <div className='ion-text-center'>
                      <IonText id='textForgotScreen5' onClick={() => history.push('/forgetPassword')}>Forgot Password?</IonText>
                    </div>
                  </IonCol>
                </IonRow>

                <IonButton className='IonButtonScreen5' expand="block" fill="solid" shape="round" id='buton' size='default' onClick={() => history.push('/Screen6')}>
                  Sign In
                </IonButton>
                <div className='ion-text-center'>
                  <IonText id='textendScreen5'>Don't Have a account? <b>Create Account</b></IonText>
                </div>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>
          </IonGrid>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Screen5;
