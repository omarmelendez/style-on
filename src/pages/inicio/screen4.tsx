import { IonBackButton, IonButton, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonPage, IonRow, IonText, IonTitle, IonToolbar } from '@ionic/react';
import { arrowForward, chevronBack, chevronForward, logoFacebook, mail, mailOpen, person, star } from 'ionicons/icons';
import { useHistory } from 'react-router';
import './screen4.css';

const Screen4: React.FC = () => {
  const history = useHistory();
  return (
    <IonPage >
      <IonHeader id='headerScreen4'>
        <IonToolbar id='toolBarScreen4'>
          <IonButton slot='start' id='buttonHeaderScreen4' onClick={() => history.push('/Screen3')}>
            <IonIcon icon={chevronBack}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <div id='backScreen4'>
          <IonGrid>

            <IonRow>
              <IonCol size="0"></IonCol>
              <IonCol size="6">
                <div className='textScreen4'>
                  Style<b>ON</b>
                </div>
              </IonCol>
              <IonCol size="6"></IonCol>
            </IonRow>

            <IonRow>
              <IonCol size="1"></IonCol>
              <IonCol size="8">
                <div className='text2Screen4'>
                  Enter the code to verify your account
                </div>
              </IonCol>
              <IonCol size="3"></IonCol>
            </IonRow>

            <IonRow>
              <IonCol size="1"></IonCol>
              <IonCol size="10">
                <div className='text3Screen4'>
                  We have sent you an SMS with a code to the number +22-714552668
                </div>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>

            <IonRow slot=''>
              <IonCol size="1"></IonCol>
              <IonCol size="10">
                <IonRow>
                  <IonCol size=''>
                    <IonItem id='itemScreen4' color='transparent'>
                      <IonLabel id='labelScreen4' color='light'></IonLabel>
                      <IonInput type='number' maxlength={3} id='inputTextScreen4'></IonInput>
                    </IonItem>
                  </IonCol>
                  <IonCol size=''>
                    <IonItem id='itemScreen4' color='transparent'>
                      <IonLabel id='labelScreen4' color='light'></IonLabel>
                      <IonInput type='number' maxlength={3} id='inputTextScreen4'></IonInput>
                    </IonItem>
                  </IonCol>
                  <IonCol size=''>
                    <IonItem id='itemScreen4' color='transparent'>
                      <IonLabel id='labelScreen4' color='light'></IonLabel>
                      <IonInput type='number' maxlength={3} id='inputTextScreen4'></IonInput>
                    </IonItem>
                  </IonCol>
                  <IonCol size=''>
                    <IonItem id='itemScreen4' color='transparent'>
                      <IonLabel id='labelScreen4' color='light'></IonLabel>
                      <IonInput type='number' maxlength={1} id='inputTextScreen4'></IonInput>
                    </IonItem>
                  </IonCol>
                </IonRow>
                <IonButton className='IonButtonScreen4' color='dark' expand="block" fill="solid" shape="round" id='buton' size='default' onClick={() => history.push('/Screen5')}>
                  <div className="textButtonScreen4">
                    <b>Verify</b>
                  </div>
                </IonButton>
                <div className='ion-text-center'>
                <IonText id='textendScreen4'>send new <b>code again?</b></IonText>
                </div>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>
          </IonGrid>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Screen4;
