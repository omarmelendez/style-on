import { IonButton, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonPage, IonRow, IonTitle, IonToolbar } from '@ionic/react';
import { arrowForward, star } from 'ionicons/icons';
import { useHistory } from 'react-router';
import './screen1.css';

const Screen1: React.FC = () => {
  const history = useHistory();
  return (
    <IonPage >
      <IonContent id='content'>
        <div id='backScreen1'>

        
        <IonGrid>

          <IonRow>
            <IonCol size="1"></IonCol>
              <IonCol size="5">
                <div className='text1'>
                  Style<b>ON</b>
                </div>
              </IonCol>
            <IonCol size="6"></IonCol>
          </IonRow>

          <IonRow>
            <IonCol size="1"></IonCol>
              <IonCol size="9">
                <div className='text2'>
                  Explore the new world of Clothing
                </div>
              </IonCol>
            <IonCol size="2"></IonCol>
          </IonRow>

          <IonRow slot=''>
            <IonCol size="1"></IonCol>
            <IonCol size="10">
              <IonButton className='IonButton' expand="block" fill="solid" shape="round" id='buton' size='large' onClick={() => history.push('/Screen2')}>
                <div className="textButton">
                  <b>Lets Explore</b>
                </div>
                  <IonIcon slot="end" icon={arrowForward} /> 
              </IonButton>
            </IonCol>
            <IonCol size="1"></IonCol>
          </IonRow>
        </IonGrid>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Screen1;
