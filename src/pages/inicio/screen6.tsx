import { menuController } from '@ionic/core';
import { IonBackButton, IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCheckbox, IonCol, IonContent, IonFab, IonFabButton, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonItemDivider, IonLabel, IonPage, IonRow, IonSegment, IonSegmentButton, IonText, IonTitle, IonToolbar } from '@ionic/react';
import { arrowForward, bagHandle, bagHandleOutline, basket, call, chevronBack, chevronForward, ellipse, globe, heart, heartCircle, heartDislike, home, key, lockClosed, logoFacebook, mail, mailOpen, menuSharp, people, peopleCircle, person, personCircle, pin, search, star } from 'ionicons/icons';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import './screen6.css';

const Screen6: React.FC = () => {
  const [like, setLike] = useState(false);
  const history = useHistory();
  let color = 'light';

  if(like) {
    color = 'danger';
  } else {
    color = 'light';
  }
  // let isFavorite = false;

  // const toggleFavorite = () => {
  //   isFavorite = !isFavorite;
  //   console.log(isFavorite);
  // }

  // const estado = (favirite:boolean) =>({
  //   if(favirite) {
  //      return {IonIcon id='iconHeart' slot="icon-only" icon={heart} color='light'></IonIcon>}
  //   }else{
  //     {<IonIcon id='iconHeart' slot="icon-only" icon={heart} color='danger'></IonIcon>}
  //   }
  // })
  // const isFavorite = favoriteSessions.indexOf(session.id) > -1;
  
  // const toggleFavorite = () => { 
  //   isFavorite ? removeFavorite(session.id) : addFavorite(session.id);

  return (
    <IonPage >
      <IonHeader id=''>
        <IonToolbar id=''>
          <IonButton slot='start' id='buttonHeaderScreen6'>
            <IonIcon id='iconHeader' icon={menuSharp}></IonIcon>
          </IonButton>
          <IonButton slot='end' id='buttonHeaderScreen6' >
            <IonIcon icon={search}></IonIcon>
          </IonButton>
          <IonButton slot='end' id='buttonHeaderScreen6'>
            <IonIcon icon={bagHandle}></IonIcon>
          </IonButton>
          <IonButton slot='end' id='buttonHeaderScreen6' >
            <IonIcon icon={personCircle}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <div id='backScreen6'>
          <IonGrid>
            <IonRow>
              <IonCol size="0"></IonCol>
              <IonCol size="10">
                <div className='textScreen6'>
                  30% off
                </div>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>
            <div id='divtextScreen6'>
              <IonRow>
                <IonCol size="0"></IonCol>
                <IonCol size="10">
                  <div className='text2Screen6'>
                    for New Users
                  </div>
                </IonCol>
                <IonCol size="1"></IonCol>
              </IonRow>
            </div>
          </IonGrid>
        </div>
        <div>
          <IonGrid>
            <div id='segment'>
              <IonSegment scrollable value="featured" color='dark'>
                <IonSegmentButton value="featured" >
                  <IonLabel>Featured</IonLabel>
                </IonSegmentButton>
                <IonSegmentButton value="newArrival">
                  <IonLabel>New Arrival</IonLabel>
                </IonSegmentButton>
                <IonSegmentButton value="summer">
                  <IonLabel>Summer</IonLabel>
                </IonSegmentButton>
                <IonSegmentButton value="winter">
                  <IonLabel>Winter</IonLabel>
                </IonSegmentButton>
                <IonSegmentButton value="blazer">
                  <IonLabel>Blazer</IonLabel>
                </IonSegmentButton>
                <IonSegmentButton value="globe">
                  <IonLabel>Featured</IonLabel>
                </IonSegmentButton>
                <IonSegmentButton value="basket">
                  <IonLabel>Featured</IonLabel>
                </IonSegmentButton>
              </IonSegment>
            </div>
          </IonGrid>
        </div>

        {/* Cards */}
        <div>
          <IonGrid>
            <IonRow>
              <IonCol size='6'>
                <IonCard >

                <IonFab vertical="top" horizontal="end" onClick={() => setLike(!like)}>
                    <IonIcon id='iconHeart' icon={heart} color={color} />
                  </IonFab>
                  <img src="assets/imgCard1.jpg" onClick={() => history.push('/productPreview')} />
                  <div id='divTitle' className='ion-text-start' >Lorem, ipsum dolorr.</div>
                  <div id='divPrecio' className='ion-text-end' >$100</div>
                  <IonRow>
                    <IonCol size='8'>
                      <IonItem id='op'>
                        <div className='ion-text-start' id='op'>
                          <IonIcon id='iconEllipse' icon={ellipse}></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='danger'></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='secondary'></IonIcon>
                        </div>
                      </IonItem>
                    </IonCol>
                    <IonCol size='4'>
                      <div className='ion-text-left'>
                        <IonButton slot='' id='buttonHeaderScreen6'>
                          <IonIcon id='icon' icon={bagHandleOutline}></IonIcon>
                        </IonButton>
                      </div>
                    </IonCol>
                  </IonRow>
                </IonCard>

              </IonCol>
              <IonCol size='6'>
                <IonCard>
                  <IonFab vertical="top" horizontal="end">
                    <IonIcon id='iconHeart' icon={heart} onClick={() => setLike(!like)} color={color} />
                  </IonFab>
                  <img src="assets/imgCard2.jpg" />
                  <div id='divTitle' className='ion-text-start' >Lorem, ipsum dolor.</div>
                  <div id='divPrecio' className='ion-text-end' >$100</div>
                  <IonRow>
                    <IonCol size='8'>
                      <IonItem id='op'>
                        <div className='ion-text-start' id='op'>
                          <IonIcon id='iconEllipse' icon={ellipse}></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='danger'></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='secondary'></IonIcon>
                        </div>
                      </IonItem>
                    </IonCol>
                    <IonCol size='4'>
                      <div className='ion-text-left'>
                        <IonButton slot='' id='buttonHeaderScreen6'>
                          <IonIcon id='icon' icon={bagHandleOutline}></IonIcon>
                        </IonButton>
                      </div>
                    </IonCol>
                  </IonRow>
                </IonCard>

              </IonCol>

            </IonRow>
            <IonRow>
              <IonCol size='6'>
                <IonCard>
                  <IonFab vertical="top" horizontal="end">
                    <IonIcon id='iconHeart' icon={heart} onClick={() => setLike(!like)} color={color} />
                  </IonFab>
                  <img src="assets/imgCard3.jpg" />
                  <div id='divTitle' className='ion-text-start' >Lorem, ipsum dolor.</div>
                  <div id='divPrecio' className='ion-text-end' >$100</div>
                  <IonRow>
                    <IonCol size='8'>
                      <IonItem id='op'>
                        <div className='ion-text-start' id='op'>
                          <IonIcon id='iconEllipse' icon={ellipse}></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='danger'></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='secondary'></IonIcon>
                        </div>
                      </IonItem>
                    </IonCol>
                    <IonCol size='4'>
                      <div className='ion-text-left'>
                        <IonButton slot='' id='buttonHeaderScreen6'>
                          <IonIcon id='icon' icon={bagHandleOutline}></IonIcon>
                        </IonButton>
                      </div>
                    </IonCol>
                  </IonRow>
                </IonCard>

              </IonCol>
              <IonCol size='6'>
                <IonCard>
                  <IonFab vertical="top" horizontal="end">
                    <IonIcon id='iconHeart' icon={heart} onClick={() => setLike(!like)} color={'light'} />
                  </IonFab>
                  <img src="assets/imgCard4.jpg" />
                  <div id='divTitle' className='ion-text-start' >Lorem, ipsum dolor.</div>
                  <div id='divPrecio' className='ion-text-end' >$100</div>
                  <IonRow>
                    <IonCol size='8'>
                      <IonItem id='op'>
                        <div className='ion-text-start' id='op'>
                          <IonIcon id='iconEllipse' icon={ellipse}></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='danger'></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='secondary'></IonIcon>
                        </div>
                      </IonItem>
                    </IonCol>
                    <IonCol size='4'>
                      <div className='ion-text-left'>
                        <IonButton slot='' id='buttonHeaderScreen6'>
                          <IonIcon id='icon' icon={bagHandleOutline}></IonIcon>
                        </IonButton>
                      </div>
                    </IonCol>
                  </IonRow>
                </IonCard>

              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol size='6'>
                <IonCard>
                  <IonFab vertical="top" horizontal="end">
                    <IonIcon id='iconHeart' icon={heart} color={heartDislike} />
                  </IonFab>
                  <img src="assets/imgCard8.jpg" />
                  <div id='divTitle' className='ion-text-start' >Lorem, ipsum dolor.</div>
                  <div id='divPrecio' className='ion-text-end' >$1000</div>
                  <IonRow>
                    <IonCol size='8'>
                      <IonItem id='op'>
                        <div className='ion-text-start' id='op'>
                          <IonIcon id='iconEllipse' icon={ellipse}></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='danger'></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='secondary'></IonIcon>
                        </div>
                      </IonItem>
                    </IonCol>
                    <IonCol size='4'>
                      <div className='ion-text-left'>
                        <IonButton slot='' id='buttonHeaderScreen6'>
                          <IonIcon id='icon' icon={bagHandleOutline}></IonIcon>
                        </IonButton>
                      </div>
                    </IonCol>
                  </IonRow>
                </IonCard>

              </IonCol>
              <IonCol size='6'>
                <IonCard>
                  <IonFab vertical="top" horizontal="end">
                    <IonIcon id='iconHeart' icon={heart} onClick={() => setLike(!like)} color={color} />
                  </IonFab>
                  <img src="assets/imgCard2.jpg" />
                  <div id='divTitle' className='ion-text-start' >Lorem, ipsum dolor.</div>
                  <div id='divPrecio' className='ion-text-end' >$100</div>
                  <IonRow>
                    <IonCol size='8'>
                      <IonItem id='op'>
                        <div className='ion-text-start' id='op'>
                          <IonIcon id='iconEllipse' icon={ellipse}></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='danger'></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='secondary'></IonIcon>
                        </div>
                      </IonItem>
                    </IonCol>
                    <IonCol size='4'>
                      <div className='ion-text-left'>
                        <IonButton slot='' id='buttonHeaderScreen6'>
                          <IonIcon id='icon' icon={bagHandleOutline}></IonIcon>
                        </IonButton>
                      </div>
                    </IonCol>
                  </IonRow>
                </IonCard>

              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol size='6'>
                <IonCard>
                  <IonFab vertical="top" horizontal="end">
                    <IonIcon id='iconHeart' icon={heart} onClick={() => setLike(!like)} color={color} />
                  </IonFab>
                  <img src="assets/imgCard7.jpg" />
                  <div id='divTitle' className='ion-text-start' >Lorem, ipsum dolor.</div>
                  <div id='divPrecio' className='ion-text-end' >$100</div>
                  <IonRow>
                    <IonCol size='8'>
                      <IonItem id='op'>
                        <div className='ion-text-start' id='op'>
                          <IonIcon id='iconEllipse' icon={ellipse}></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='danger'></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='secondary'></IonIcon>
                        </div>
                      </IonItem>
                    </IonCol>
                    <IonCol size='4'>
                      <div className='ion-text-left'>
                        <IonButton slot='' id='buttonHeaderScreen6'>
                          <IonIcon id='icon' icon={bagHandleOutline}></IonIcon>
                        </IonButton>
                      </div>
                    </IonCol>
                  </IonRow>
                </IonCard>

              </IonCol>
              <IonCol size='6'>
                <IonCard>
                  <IonFab vertical="top" horizontal="end">
                    <IonIcon id='iconHeart' icon={heart} onClick={() => setLike(!like)} color={color} />
                  </IonFab>
                  <img src="assets/imgCard5.jpg" />
                  <div id='divTitle' className='ion-text-start' >Lorem, ipsum dolor.</div>
                  <div id='divPrecio' className='ion-text-end' >$100</div>
                  <IonRow>
                    <IonCol size='8'>
                      <IonItem id='op'>
                        <div className='ion-text-start' id='op'>
                          <IonIcon id='iconEllipse' icon={ellipse}></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='danger'></IonIcon>
                          <IonIcon id='iconEllipse' icon={ellipse} color='secondary'></IonIcon>
                        </div>
                      </IonItem>
                    </IonCol>
                    <IonCol size='4'>
                      <div className='ion-text-left'>
                        <IonButton slot='' id='buttonHeaderScreen6'>
                          <IonIcon id='icon' icon={bagHandleOutline}></IonIcon>
                        </IonButton>
                      </div>
                    </IonCol>
                  </IonRow>
                </IonCard>

              </IonCol>
              {/* <IonCol size='6'>
                <IonCard>
                  <img src="assets/imgCard2.jpg" />
                  <IonCardHeader>
                    <IonCardSubtitle>Destination</IonCardSubtitle>
                    <IonCardTitle>Madison, WI</IonCardTitle>
                  </IonCardHeader>
                  <IonItem>
                    <IonButton slot='end' id='buttonHeaderScreen6'>
                      <IonIcon icon={home}></IonIcon>
                    </IonButton>
                  </IonItem>
                </IonCard>
              </IonCol> */}
            </IonRow>
          </IonGrid>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Screen6;

function toggleFavorite(): void {
  throw new Error('Function not implemented.');
}
