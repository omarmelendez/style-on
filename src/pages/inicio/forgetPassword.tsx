import { IonBackButton, IonButton, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonPage, IonRow, IonText, IonTitle, IonToolbar } from '@ionic/react';
import { arrowForward, chevronBack, chevronForward, logoFacebook, mail, mailOpen, person, phoneLandscape, phonePortraitOutline, phonePortraitSharp, star } from 'ionicons/icons';
import { useHistory } from 'react-router';
import './forgetPassword.css';

const ForgetPassword: React.FC = () => {
  const history = useHistory();
  return (
    <IonPage >
      <IonHeader id='headerForgetPassword'>
        <IonToolbar id='toolBarForgetPassword'>
          <IonButton slot='start' id='buttonHeaderForgetPassword' onClick={() => history.push('/Screen3')}>
            <IonIcon icon={chevronBack}></IonIcon>
          </IonButton>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <div id='backScreen4'>
          <IonGrid>

            <IonRow>
              <IonCol size="0"></IonCol>
              <IonCol size="6">
                <div className='textForgetPassword'>
                  Style<b>ON</b>
                </div>
              </IonCol>
              <IonCol size="6"></IonCol>
            </IonRow>

            <IonRow>
              <IonCol size="0"></IonCol>
              <IonCol size="8">
                <div className='text2ForgetPassword'>
                  Forget Password?
                </div>
              </IonCol>
              <IonCol size="3"></IonCol>
            </IonRow>

            <IonRow>
              <IonCol size="0"></IonCol>
              <IonCol size="10">
                <div className='text3ForgetPassword'>
                  if you forget your account password phone enter your Registered number
                </div>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>

            <IonRow slot=''>
              <IonCol size="1"></IonCol>
              <IonCol size="10">
                <IonItem id='itemForgetPassword' color='transparent'>
                  <IonLabel id='labelForgetPassword' color='light'><IonIcon icon={phonePortraitSharp} color='dark'></IonIcon></IonLabel>
                  <IonInput placeholder='Enter Registered number' id='inputTextForgetPassword'></IonInput>
                </IonItem>
                <div className='ion-text-center'>
                <IonText id='textEndForgetPassword'><b>Enter OP</b></IonText>
                </div>
                <IonRow>
                  <IonCol size=''>
                    <IonItem id='itemForgetPassword' color='transparent'>
                      <IonLabel id='labelForgetPassword' color='dark'></IonLabel>
                      <IonInput type='number' maxlength={3} id='inputTextScreen4'></IonInput>
                    </IonItem>
                  </IonCol>
                  <IonCol size=''>
                    <IonItem id='itemForgetPassword' color='transparent'>
                      <IonLabel id='labelForgetPassword' color='light'></IonLabel>
                      <IonInput type='number' maxlength={3} id='inputTextForgetPassword'></IonInput>
                    </IonItem>
                  </IonCol>
                  <IonCol size=''>
                    <IonItem id='itemForgetPassword' color='transparent'>
                      <IonLabel id='labelForgetPassword' color='light'></IonLabel>
                      <IonInput type='number' maxlength={3} id='inputTextForgetPassword'></IonInput>
                    </IonItem>
                  </IonCol>
                  <IonCol size=''>
                    <IonItem id='itemForgetPassword' color='transparent'>
                      <IonLabel id='labelForgetPassword' color='light'></IonLabel>
                      <IonInput type='number' maxlength={1} id='inputTextForgetPassword'></IonInput>
                    </IonItem>
                  </IonCol>
                </IonRow>
                <IonButton className='IonButtonForgetPassword' color='dark' expand="block" fill="solid" shape="round" id='buton' size='default' onClick={() => history.push('/Screen5')}>
                  <div className="textButtonForgetPassword">
                    <b>Send</b>
                  </div>
                </IonButton>
              </IonCol>
              <IonCol size="1"></IonCol>
            </IonRow>
          </IonGrid>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default ForgetPassword;
