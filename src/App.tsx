import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  setupIonicReact
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { heart, heartOutline, home, notifications, personCircle} from 'ionicons/icons';
import Tab1 from './pages/Tab1';
import Tab2 from './pages/Tab2';
import Tab3 from './pages/Tab3';
import Tab4 from './pages/Tab4';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import Screen1 from './pages/inicio/screen1';
import React from 'react';
import Screen2 from './pages/inicio/screen2';
import Screen3 from './pages/inicio/screen3';
import Screen4 from './pages/inicio/screen4';
import Screen5 from './pages/inicio/screen5';
import screen6 from './pages/inicio/screen6';
import ForgetPassword from './pages/inicio/forgetPassword';
import ProductPreview from './pages/inicio/productPreview';
import CartList from './pages/cart/cardList';
import Shipping from './pages/pay/shipping';



setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      
      <IonTabs>
        <IonRouterOutlet>
          <Route exact path="/tab1">
            <Tab1 />
          </Route>
          <Route exact path="/tab2">
            <Tab2 />
          </Route>
          <Route path="/tab3">
            <Tab3 />
          </Route>
          <Route path="/tab4">
            <Tab4 />
          </Route>
          <Route path="/screen1" component={Screen1} exact/>
          <Route path="/screen2" component={Screen2} exact/>
          <Route path="/screen3" component={Screen3} exact/>
          <Route path="/screen4" component={Screen4} exact/>
          <Route path="/screen5" component={Screen5} exact/>
          <Route path="/screen6" component={screen6} exact/>
          <Route path="/forgetPassword" component={ForgetPassword} exact/>
          <Route path="/productPreview" component={ProductPreview} exact/>
          <Route path="/card-List" component={CartList} exact/>
          <Route path="/shipping" component={Shipping} exact/>
          <Route exact path="/">
            <Redirect to="/screen1" />
          </Route>
        </IonRouterOutlet>
        <IonTabBar slot="bottom" color='primary' id='ionTabBar' hidden>
          <IonTabButton tab="tab1" href="/tab1">
            <IonIcon icon={home} />
            {/* <IonLabel>Tab 1</IonLabel> */}
          </IonTabButton>
          <IonTabButton tab="tab2" href="/tab2">
            <IonIcon icon={notifications} />
            {/* <IonLabel>Tab 2</IonLabel> */}
          </IonTabButton>
          <IonTabButton tab="tab3" href="/tab3">
            <IonIcon icon={heart} />
            {/* <IonLabel>Tab 3</IonLabel> */}
          </IonTabButton>
          <IonTabButton tab="tab4" href="/tab4">
            <IonIcon icon={personCircle} />
            {/* <IonLabel>Tab 4</IonLabel> */}
          </IonTabButton>
        </IonTabBar>
        
      </IonTabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
